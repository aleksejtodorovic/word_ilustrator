import axios from 'axios';

const UNSPLACH_KEY = '13fe49e8067733fbe93daff752f5715d57c614eaf7021a0c3fe98bc1f1b98ee8';

export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers: {
        Authorization: `Client-ID ${UNSPLACH_KEY}`
    }
});